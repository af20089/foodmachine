import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class simplefoodmachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JTextPane receivedInfo;
    private JButton checkOutButton;
    private JTextPane totalyen;

    public simplefoodmachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Tempura",100);}
            });
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("foodimages\\tempura.jpg")
        ));

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("karaage",200);}
            });
        karaageButton.setIcon(new ImageIcon(this.getClass().getResource("foodimages\\karaage.jpg")
        ));

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("gyoza",300);}
        });
        gyozaButton.setIcon(new ImageIcon(this.getClass().getResource("foodimages\\gyoza.jpg")
        ));

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("udon",400);}
        });
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("foodimages\\udon.jpg")
        ));

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("yakisoba",500);}
        });
        yakisobaButton.setIcon(new ImageIcon(this.getClass().getResource("foodimages\\yakisoba.jpg")
        ));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("ramen",600);}
        });
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("foodimages\\ramen.jpg")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(
                        null ,
                        "Would you like to check out?",
                        "Check out Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + total + "yen");
                    total = 0;
                    receivedInfo.setText("");
                    totalyen.setText("total 0yen");
                }
            }
        });
    }

    int total=0;
    void order (String food,int price){
            int confirmation=JOptionPane.showConfirmDialog(
                    null ,
                    "Would you like to order"+food+"?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            if(confirmation==0){
                JOptionPane.showMessageDialog (null ,"Thank you for ordering "+food+"! It will be served as soon as possible");
                String currentText = receivedInfo.getText();
                receivedInfo.setText(currentText +food+"\n");
                total+=price;
                totalyen.setText("total "+total+" yen");
            }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("simplefoodmachine");
        frame.setContentPane(new simplefoodmachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
